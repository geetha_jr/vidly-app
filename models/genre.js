const Joi = require('joi');
const mongoose = require('mongoose');

const Genre = new mongoose.model('Genre', new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 20
  }
}));

function validateSchema(genre) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(20).required()
  });
  return schema.validate(genre);
};

module.exports.Genre = Genre;
module.exports.validate = validateSchema;