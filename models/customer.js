const Joi = require('joi');
const mongoose = require('mongoose');

const Customer = new mongoose.model('Customer', new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 20
  },
  isGold: {
    type: Boolean,
    default: false
  },
  phone: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 20
  }
}));

function validateSchema(customer) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(20).required(),
    phone: Joi.string().min(5).max(20),
    isGold: Boolean
  });
  return schema.validate(customer);
};

module.exports.Customer = Customer;
module.exports.validate = validateSchema;