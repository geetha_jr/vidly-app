const express = require('express')
const {Genre, validate} = require('../models/genre');

const router = express.Router();

router.get('/', async (req, res) => {
  const results = await Genre.find();
  res.send(results);
});

router.get('/:id', async (req, res) => {
  try {
    const result = await Genre.findById(req.params.id);
    if (!result) res.status(404).send("Genre with given id is not available!");
    
    res.send(result);
  } catch (err) {
    res.status(400).send(err.message);
  }
});

router.post('/', async (req, res) => {
  const {error, value} = validate(req.body);
  if (error) {
    res.status(400).send(error.details[0].message)
  }

  const genre = new Genre({
    name: req.body.name
  });

  try {
    const result = await genre.save();
    res.send(result);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

router.put('/:id', async (req, res) => {
  const {error, value} = (req.body);
  if (error) {
    let msg = ""
    error.details.forEach(e => {msg = msg + e.message})
    res.status(400).send(msg);
  }

  try {
    const result = await Genre.findOneAndUpdate({_id: req.params.id}, {name: req.body.name}, {new: true});
    if (result) {
      res.send(result);
    } else {
      res.status(404).send("Genre with given id is not available!")
    }
  } catch (err) {
    res.status(500).send(err.message);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const result = await Genre.findByIdAndRemove(req.params.id);
    if (!result) {res.status(404).send("Genre with the given id is not available!")}
    res.send(result);
  } catch (err) {
    res.status(400).send(err.message);
  }
});

module.exports = router;