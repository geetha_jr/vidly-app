const express = require('express');
const mongoose = require('mongoose');
const home = require('./routes/home');
const genres = require('./routes/genres');
const customers = require('./routes/customers');

const app = express();

async function connectToDB() {
  try {
    await mongoose.connect('mongodb://localhost:27017/vidly');
    console.log('Connected to vidly database');
  } catch (err) {
    console.error(err.message);
  }
}

connectToDB();

app.use(express.json());
app.use(express.static("public"));
app.use('/', home);
app.use("/api/genres", genres);
app.use("/api/customers", customers);

app.listen(3000, () => console.log("App Listening on port 3000"));